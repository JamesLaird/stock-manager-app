import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StockManagerService {

  private API = '//localhost:8080/';
  private PRODUCT_API = this.API + '/product';
  private STOCK_DOWNLOAD_API = this.API + '/download';

  constructor(private http: HttpClient) {
  }

  getAll(endpoint: string): Observable<any> {
    return this.http.get(this.API + endpoint).pipe(
      catchError(this.handleError(this.API + endpoint, []))
    );
  }

  downloadStockDetails(): Observable<any> {
    return this.http.get(this.STOCK_DOWNLOAD_API).pipe(
      catchError(this.handleError(this.STOCK_DOWNLOAD_API, []))
    );
  }

  addProduct(product: any): Observable<any> {
    const result = this.http.post(this.PRODUCT_API, product);
    return result;
  }

  updateProduct(product: any): Observable<any> {
    const result = this.http.post(this.PRODUCT_API, product);
    return result;
  }

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console
      return of(result as T);
    };
  }
}
