import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StockDetailsComponent } from './stock-details/stock-details.component';
import { StockAuditHistoryComponent } from './stock-audit-history/stock-audit-history.component';

const routes: Routes = [
  { path: '', redirectTo: '/stock-details', pathMatch: 'full' },
  {
    path: 'stock-details',
    component: StockDetailsComponent
  },
  {
    path: 'stock-audit-history',
    component: StockAuditHistoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
