import {Component, OnInit, ViewChild} from '@angular/core';
import {StockManagerService} from '../shared/stock-manager.service';
import {DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-stock-details',
  templateUrl: './stock-details.component.html',
  styleUrls: ['./stock-details.component.css']
})
export class StockDetailsComponent implements OnInit {

  stockCheckEndpoint = 'stockCheck';
  downloadJsonHref: SafeUrl;

  displayedColumns: string[] = ['image', 'name', 'stockAmount', 'minStockLevel', 'oneOffOrderAmount', 'recommendedStockAction'];

  dataSource: any;

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private stockManagerService: StockManagerService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.stockManagerService.getAll(this.stockCheckEndpoint).subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.downloadStockDetails();
    });
  }

  downloadStockDetails() {
    this.stockManagerService.getAll(this.stockCheckEndpoint).subscribe(response => {
      const theJSON = JSON.stringify(response);
      const uri = this.sanitizer.bypassSecurityTrustUrl('data:text/json;charset=UTF-8,' + encodeURIComponent(theJSON));
      this.downloadJsonHref = uri;
    });
  }

}
