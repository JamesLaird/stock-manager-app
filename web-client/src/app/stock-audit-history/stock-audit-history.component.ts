import {Component, OnInit, ViewChild} from '@angular/core';
import {StockManagerService} from '../shared/stock-manager.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-stock-audit-history',
  templateUrl: './stock-audit-history.component.html',
  styleUrls: ['./stock-audit-history.component.css']
})
export class StockAuditHistoryComponent implements OnInit {

  stockAuditEndpoint = 'stockAudit';

  displayedColumns: string[] = ['name', 'recommendedAction', 'recommendActionTimestamp'];

  dataSource: any;

  downloadJsonHref: SafeUrl;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private stockManagerService: StockManagerService, private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.stockManagerService.getAll(this.stockAuditEndpoint).subscribe(data => {
      this.dataSource = new MatTableDataSource<StockElement>(data);
      this.dataSource.paginator = this.paginator;
      this.downloadStockAuditHistory();
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  downloadStockAuditHistory() {
    this.stockManagerService.getAll(this.stockAuditEndpoint).subscribe(response => {
      const theJSON = JSON.stringify(response);
      const uri = this.sanitizer.bypassSecurityTrustUrl('data:text/json;charset=UTF-8,' + encodeURIComponent(theJSON));
      this.downloadJsonHref = uri;
    });
  }
}

export interface StockElement {
  productName: string;
  stockRecommendAction: string;
  recommendActionTimestamp: string;
}
