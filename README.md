## Stock Manager Application

This is a Java/Angular web application that provides a user interface to 

- list all stock details 

- view stock audit history

- search stock audit history by product

- download the current stock details and audit history as a JSON file

It makes REST calls to a Stock Manager Spring Boot service.

Java REST services allow you to:

- Add a product

- Edit a product

- Query for stock details

- Query the stock audit history


## Running Spring Boot Service

### Prerequisites

- Java 8
- Maven
- Node

### Loading into IntelliJ
From the main IntelliJ startup menu - Import Project > Import project from external model > Maven

or from an opened IntelliJ - Top Menu > File > New > Project from Existing Sources (select directory) > Import project from external model > Maven

- This project uses Lombok (https://projectlombok.org/) to generate the entities get and setters. 

You need to enable this in IntelliJ:
https://stackoverflow.com/questions/9424364/cant-compile-project-when-im-using-lombok-under-intellij-idea


### API Docs
The rest API docs can be viewed here:

http://localhost:8080/swagger-ui.html

### Logging
The Logging framework used in this code is SLF4J. The debug logging is output at INFO level.

This can be changed in the application.properties if required.

### Usage
- This Spring Boot application is configured to listen on port 8080
- The tomcat logs directory is /tmp/tomcat/stockmanager

### Maven commands (from a command prompt or IntelliJ terminal window)

If running from the command prompt or terminal, go to the project's root folder, then type:

./mvnw clean package 						(to build the application)
    
./mvnw spring-boot:run 						(to start the application)

./mvnw clean verify  						(to run unit and integration tests)

## Angular Web Client
See the README file in the root of the web-client directory for how the Angular app gets built and deployed


## Project Next Steps/Future Work
This is just a MVP application - to get this product production ready we would have to consider:

- Security and authentication via Okta or a similar security framework for the Angular-app and REST services

- Role based access

- Filtering and sorting order

- A notification service when stock is below minimum threshold 

- Styling/branding 

- Usability and accessibility

- Allowing the Angular app to add new products and update stock details via the REST endpoints

- Build out an automated test framework for the web client and REST services

- Selecting a more robust database framework



