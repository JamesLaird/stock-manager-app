package com.myretailer.stockmanager.jpa;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "STOCK_CHECK_AUDIT", uniqueConstraints = {@UniqueConstraint(columnNames = "ID")})
@Getter
@Setter
public class StockCheckAuditEntity implements Serializable {

    private static final long serialVersionUID = -1798070786993154676L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long stockAuditId;

    @Column(name = "PRODUCT_ID", nullable = false)
    private Long productId;

    @Column(name = "PRODUCT_NAME", nullable = false, length = 100)
    private String productName;

    @Column(name = "STOCK_RECOMMEND_ACTION", nullable = false, length = 100)
    private String stockRecommendAction;

    @Column(name = "RECOMMEND_ACTION_TIMESTAMP", nullable = false)
    private Date recommendActionTimestamp;

}
