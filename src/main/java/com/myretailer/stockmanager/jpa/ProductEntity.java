package com.myretailer.stockmanager.jpa;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "PRODUCT_LIST", uniqueConstraints = {@UniqueConstraint(columnNames = "ID"), @UniqueConstraint(columnNames = "NAME")})
@Getter
@Setter
public class ProductEntity implements Serializable {

    private static final long serialVersionUID = -1798070786993154676L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long productId;

    @NotBlank(message = "Product name is required")
    @Size(max = 100, message = "Product name cannot be greater than 100 characters in length")
    @Column(name = "NAME", unique = true, nullable = false, length = 100)
    private String name;

    @NotBlank(message = "Product image is required")
    @Size(max = 500, message = "Product image cannot be greater than 500 characters in length")
    @Column(name = "IMAGE", nullable = false, length = 500)
    private String image;

    @NotNull(message = "Current stock level is required")
    @Column(name = "CURRENT_STOCK_LEVEL", nullable = false)
    private Long currentStockLevel;

    @NotNull(message = "Stock minimum stock level is required")
    @Column(name = "MIN_STOCK_LEVEL", nullable = false)
    private Long minStockLevel;

    @Column(name = "REORDER_BLOCKED", nullable = false)
    private Boolean reorderBlocked;

    @NotNull(message = "Stock one off order amount is required")
    @Column(name = "ONE_OFF_ORDER_AMOUNT", nullable = false)
    private Long oneOffOrderAmount;

    @Transient
    private String recommendedStockAction;
}