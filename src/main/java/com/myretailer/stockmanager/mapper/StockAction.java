package com.myretailer.stockmanager.mapper;

enum StockAction {

    ONE_OFF_STOCK_ORDER("One Off Stock Order"),
    ORDER_NEW_STOCK("Order New Stock"),
    STOCK_LEVEL_OK("Stock Level Ok"),
    STOCK_ORDER_BLOCKED("Stock Order Blocked");

    private String action;

    StockAction(final String action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return action;
    }

}

