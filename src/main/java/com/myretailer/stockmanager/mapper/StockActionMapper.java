package com.myretailer.stockmanager.mapper;

import com.myretailer.stockmanager.jpa.ProductEntity;

public class StockActionMapper {

    /*
     * Map stock action logic would have to be confirmed by a BA or product owner
     *
     * Currently we prioritise:
     * 1) Stock reorder blocked
     * 2) Stock level is below min amount
     * 3) Stock has one off order flag
     * 4) No action required
     *
     * */
    public static String mapStockAction(ProductEntity product) {
        String stockAction;
        if (product.getReorderBlocked()) {
            stockAction = StockAction.STOCK_ORDER_BLOCKED.toString();
        } else if (product.getCurrentStockLevel() <= product.getMinStockLevel()) {
            stockAction = StockAction.ORDER_NEW_STOCK.toString();
        } else if (product.getOneOffOrderAmount() > 0) {
            stockAction = StockAction.ONE_OFF_STOCK_ORDER.toString();
        } else {
            stockAction = StockAction.STOCK_LEVEL_OK.toString();
        }

        return stockAction;
    }
}
