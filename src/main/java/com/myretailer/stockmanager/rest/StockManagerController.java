package com.myretailer.stockmanager.rest;

import com.myretailer.stockmanager.error.ProductErrorResponse;
import com.myretailer.stockmanager.jpa.ProductEntity;
import com.myretailer.stockmanager.jpa.StockCheckAuditEntity;
import com.myretailer.stockmanager.service.StockServiceImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Collection;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
class StockManagerController {

    @Autowired
    StockServiceImpl stockService;

    @GetMapping("/stockCheck")
    public Collection<ProductEntity> getStockCheck() {
        return stockService.getStockCheck();
    }

    @PostMapping("/addProduct")
    public ResponseEntity<?> addProduct(@NotNull @RequestBody ProductEntity product) {

        if (stockService.productAlreadyAdded(product.getName())) {
            return new ResponseEntity(new ProductErrorResponse("Unable to create. A product with the name " +
                    product.getName() + " already exists."), HttpStatus.CONFLICT);
        }
        stockService.saveProduct(product);

        return new ResponseEntity<String>(HttpStatus.CREATED);
    }

    @PutMapping("/updateProduct")
    public ResponseEntity<?> updateProduct(@RequestBody ProductEntity product) {

        stockService.updateProduct(product);

        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @GetMapping("/stockAudit")
    public Collection<StockCheckAuditEntity> getStockAudit() {
        return stockService.getStockAuditDetails();
    }


}