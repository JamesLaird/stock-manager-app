package com.myretailer.stockmanager;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.myretailer.stockmanager.jpa.ProductEntity;
import com.myretailer.stockmanager.jpa.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

@SpringBootApplication(exclude = {RepositoryRestMvcAutoConfiguration.class})
public class StockManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockManagerApplication.class, args);
    }

    private static final Logger logger = LoggerFactory.getLogger(StockManagerApplication.class);

    private static final String JSON_DATA_URL = "stock-list.json";

    private static final String STOCK_ORDER_BLOCKED = "Yes";

    @Bean
    ApplicationRunner init(ProductRepository repository) {
        return args -> {
            logger.info("Starting loading stock products");
            try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(JSON_DATA_URL)) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                StringBuffer buffer = new StringBuffer();
                String line = reader.readLine();
                while (line != null) {
                    buffer.append(line);
                    line = reader.readLine();
                }

                logger.debug("Parsing products json");
                JsonParser parser = new JsonFactory().createParser(buffer.toString());
                if (JsonToken.START_ARRAY != parser.nextToken()) {
                    throw new Exception("bad token");
                }

                JsonToken nextToken = parser.nextToken();
                while (nextToken == JsonToken.START_OBJECT) {

                    ProductEntity productEntity = new ProductEntity();
                    logger.debug(parser.nextFieldName());
                    productEntity.setName(parser.nextTextValue());

                    logger.debug(parser.nextFieldName());
                    productEntity.setImage(parser.nextTextValue());

                    logger.debug(parser.nextFieldName());
                    productEntity.setCurrentStockLevel(Long.parseLong(parser.nextTextValue()));

                    logger.debug(parser.nextFieldName());
                    productEntity.setMinStockLevel(Long.parseLong(parser.nextTextValue()));

                    logger.debug(parser.nextFieldName());
                    productEntity.setOneOffOrderAmount(Long.parseLong(parser.nextTextValue()));

                    logger.debug(parser.nextFieldName());
                    productEntity.setReorderBlocked(parser.nextTextValue().equalsIgnoreCase(STOCK_ORDER_BLOCKED)  ? true : false);

                    repository.save(productEntity);

                    nextToken = parser.nextToken();
                    logger.debug(String.valueOf(nextToken));

                    nextToken = parser.nextToken();
                    logger.debug(String.valueOf(nextToken));
                }

            } catch (Exception ex) {
                logger.error("Loading stock products json error");
                throw new Exception(ex);
            }
            logger.info("Finished loading stock products successfully");
        };
    }

}
