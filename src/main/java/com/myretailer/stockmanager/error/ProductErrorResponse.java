package com.myretailer.stockmanager.error;

public class ProductErrorResponse {

    private String error;

    public ProductErrorResponse(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}
