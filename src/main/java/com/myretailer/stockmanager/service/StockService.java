package com.myretailer.stockmanager.service;

import com.myretailer.stockmanager.jpa.ProductEntity;
import com.myretailer.stockmanager.jpa.StockCheckAuditEntity;

import java.util.List;

public interface StockService {

    List<ProductEntity> getStockCheck();

    boolean productAlreadyAdded(String stock);

    void saveProduct(ProductEntity cake);

    void updateProduct(ProductEntity cake);

    void saveStockCheckAuditDetails(List<ProductEntity> stockList);

    List<StockCheckAuditEntity> getStockAuditDetails();
}
