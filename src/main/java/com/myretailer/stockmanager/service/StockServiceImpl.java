package com.myretailer.stockmanager.service;

import com.myretailer.stockmanager.jpa.ProductEntity;
import com.myretailer.stockmanager.jpa.ProductRepository;
import com.myretailer.stockmanager.jpa.StockCheckAuditEntity;
import com.myretailer.stockmanager.jpa.StockCheckAuditRepository;
import com.myretailer.stockmanager.mapper.StockActionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class StockServiceImpl implements StockService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private StockCheckAuditRepository stockAuditRepository;

    @Override
    public List<ProductEntity> getStockCheck() {

        List<ProductEntity> productList = productRepository.findAll();

        saveStockCheckAuditDetails(productList);

        return new ArrayList<>(productList);
    }


    @Override
    public boolean productAlreadyAdded(String productName) {

        return !productRepository.findByName(productName).isEmpty();
    }

    @Override
    public void saveProduct(ProductEntity product) {
        productRepository.save(product);
    }

    @Override
    public void updateProduct(ProductEntity product) { productRepository.save(product);
    }

    @Override
    public void saveStockCheckAuditDetails(List<ProductEntity> productList) {
        productList.forEach(product -> {

            // set the recommended stock action value and ensure this is persisted to the Stock Audit history
            product.setRecommendedStockAction(StockActionMapper.mapStockAction(product));

            StockCheckAuditEntity sae = new StockCheckAuditEntity();
            sae.setRecommendActionTimestamp(new Date());
            sae.setProductId(product.getProductId());
            sae.setProductName(product.getName());
            sae.setStockRecommendAction(product.getRecommendedStockAction());
            stockAuditRepository.save(sae);
        });

    }

    @Override
    public List<StockCheckAuditEntity> getStockAuditDetails() {
        return new ArrayList<>(stockAuditRepository.findAll());
    }
}
