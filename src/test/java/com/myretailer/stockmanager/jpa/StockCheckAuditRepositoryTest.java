package com.myretailer.stockmanager.jpa;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StockCheckAuditRepositoryTest {

    @Autowired
    private StockCheckAuditRepository stockCheckAuditRepository;

    @Test
    public void whenFindingStockCheckAuditById_thenCorrect() {
        StockCheckAuditEntity stockCheckAudit = new StockCheckAuditEntity();
        stockCheckAudit.setProductName("Bran Flakes");
        stockCheckAudit.setProductId(1L);
        stockCheckAudit.setRecommendActionTimestamp(new Date());
        stockCheckAudit.setStockRecommendAction("Buy Bran Flakes");
        stockCheckAuditRepository.save(stockCheckAudit);
        assertThat(stockCheckAuditRepository.findById(1L)).isInstanceOf(Optional.class);
    }

    @Test
    public void whenFindingAllStockCheckAudits_thenCorrect() {
        StockCheckAuditEntity stockCheckAuditOne = new StockCheckAuditEntity();
        stockCheckAuditOne.setProductName("Bran Flakes");
        stockCheckAuditOne.setProductId(1L);
        stockCheckAuditOne.setRecommendActionTimestamp(new Date());
        stockCheckAuditOne.setStockRecommendAction("Buy Bran Flakes");
        StockCheckAuditEntity stockCheckAuditTwo = new StockCheckAuditEntity();
        stockCheckAuditTwo.setProductName("Other Flakes");
        stockCheckAuditTwo.setProductId(2L);
        stockCheckAuditTwo.setRecommendActionTimestamp(new Date());
        stockCheckAuditTwo.setStockRecommendAction("Stock OK");
        stockCheckAuditRepository.save(stockCheckAuditOne);
        stockCheckAuditRepository.save(stockCheckAuditTwo);
        assertThat(stockCheckAuditRepository.findAll()).isInstanceOf(List.class);
    }
}