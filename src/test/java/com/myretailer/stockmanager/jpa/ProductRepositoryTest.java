package com.myretailer.stockmanager.jpa;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void whenFindingProductById_thenCorrect() {
        ProductEntity product = new ProductEntity();
        product.setName("Bran Flakes");
        product.setImage("branFlakes.jpg");
        product.setMinStockLevel(100L);
        product.setOneOffOrderAmount(0L);
        product.setCurrentStockLevel(200L);
        product.setReorderBlocked(false);
        productRepository.save(product);
        assertThat(productRepository.findById(1L)).isInstanceOf(Optional.class);
    }

    @Test
    public void whenFindingAllProducts_thenCorrect() {
        ProductEntity productOne = new ProductEntity();
        productOne.setName("Bran flakes");
        productOne.setImage("branFlakes.jpg");
        productOne.setMinStockLevel(100L);
        productOne.setOneOffOrderAmount(0L);
        productOne.setReorderBlocked(false);
        productOne.setCurrentStockLevel(200L);
        ProductEntity productTwo = new ProductEntity();
        productTwo.setName("Shreddies");
        productTwo.setImage("shreddies.jpg");
        productTwo.setMinStockLevel(100L);
        productTwo.setOneOffOrderAmount(0L);
        productTwo.setReorderBlocked(true);
        productTwo.setCurrentStockLevel(200L);
        productRepository.save(productOne);
        productRepository.save(productTwo);
        assertThat(productRepository.findAll()).isInstanceOf(List.class);
    }
}