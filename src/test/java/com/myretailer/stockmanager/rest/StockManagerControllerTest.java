package com.myretailer.stockmanager.rest;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.myretailer.stockmanager.jpa.ProductEntity;
import com.myretailer.stockmanager.jpa.StockCheckAuditEntity;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.util.NestedServletException;

public class StockManagerControllerTest extends AbstractTest {
    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void getStockCheck() throws Exception {
        String uri = "/stockCheck";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        ProductEntity[] productsList = super.mapFromJson(content, ProductEntity[].class);
        assertTrue(productsList.length > 0);
    }

    @Test
    public void getStockAudit() throws Exception {
        String stockCheckUri = "/stockCheck";
       mvc.perform(MockMvcRequestBuilders.get(stockCheckUri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        String stockAuditUri = "/stockAudit";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(stockAuditUri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        StockCheckAuditEntity[] stockAuditList = super.mapFromJson(content, StockCheckAuditEntity[].class);
        assertTrue(stockAuditList.length > 0);
    }


    @Test
    public void addProduct() throws Exception {
        String uri = "/addProduct";
        ProductEntity product = new ProductEntity();
        product.setName("Mars Bar");
        product.setImage("mars.jpg");
        product.setMinStockLevel(100L);
        product.setOneOffOrderAmount(0L);
        product.setReorderBlocked(false);
        product.setCurrentStockLevel(200L);
        String inputJson = super.mapToJson(product);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
    }

    @Test
    public void updateProduct() throws Exception {
        String uri = "/updateProduct";
        ProductEntity product = new ProductEntity();
        product.setProductId(1L);
        product.setName("Update Mars Bar");
        product.setImage("mars.jpg");
        product.setMinStockLevel(100L);
        product.setOneOffOrderAmount(0L);
        product.setReorderBlocked(true);
        product.setCurrentStockLevel(200L);
        String inputJson = super.mapToJson(product);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
    }

    @Test
    public void addDuplicateProduct() throws Exception {
        String uri = "/addProduct";
        ProductEntity product = new ProductEntity();
        product.setName("Mars Bar");
        product.setImage("product.jpg");
        product.setMinStockLevel(100L);
        product.setOneOffOrderAmount(0L);
        product.setReorderBlocked(false);
        product.setCurrentStockLevel(200L);
        String inputJson = super.mapToJson(product);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(409, status);
    }

    @Test(expected = org.springframework.web.util.NestedServletException.class)
    public void addProductNoName_expectException() throws Exception {
        String uri = "/addProduct";
        ProductEntity product = new ProductEntity();
        product.setName(null);
        product.setImage("product.jpg");
        String inputJson = super.mapToJson(product);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        exceptionRule.expect(NestedServletException.class);
        exceptionRule.expectMessage("Product title is required");

    }


    @Test(expected = org.springframework.web.util.NestedServletException.class)
    public void addProductNoImage_expectException() throws Exception {
        String uri = "/addProduct";
        ProductEntity product = new ProductEntity();
        product.setName("product");
        product.setImage(null);
        String inputJson = super.mapToJson(product);
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();
        exceptionRule.expect(NestedServletException.class);
        exceptionRule.expectMessage("Product image is required");

    }

}
